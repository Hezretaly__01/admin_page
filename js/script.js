var dropdown = document.querySelectorAll(".popup-apis__select-button");
var dropdownBody = document.querySelector(".popup-apis__select-body");
dropdown.forEach((element) => {
  element.addEventListener("click", () => {
    element.nextElementSibling.classList.toggle("active");
  });
});

//==================

var closeBtn = document.querySelector(".popup-apis__close-box");
var apiPopUp = document.querySelector(".popup-apis");
var addApi = document.querySelector(".right-apis__add");
var editApi = document.querySelector(".right-apis__edit");
closeBtn.addEventListener('click', () => {
	apiPopUp.classList.add('_hide');
})
addApi.addEventListener('click', () => {
	if (apiPopUp.classList.contains("_hide")) {
		apiPopUp.classList.remove("_hide");
	}
});
editApi.addEventListener('click', () => {
	if (apiPopUp.classList.contains("_hide")) {
		apiPopUp.classList.remove("_hide");
	}
});
